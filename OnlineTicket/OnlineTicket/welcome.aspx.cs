﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace onlineTicket
{
    public partial class welcome : System.Web.UI.Page
    {

        public string ConnectionStringNesnesi()
        {
            try
            {
                string Providers = "Data Source=MINE-PC/MINE;Initial Catalog=onlineTicket;Integrated Security=True";
                return Providers;
            }
            catch (Exception ex)
            {
                Response.Write("DATABASE CONNECTİON HATA \n ------------------------------------------------------------------" + ex.ToString() + "\n ------------------------------------------------------------------");
                return null;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["kulAdi"] == null)
            {
                Response.Redirect("hata.aspx");
            }
            else
            {
                Label2.Text = Session["kulAdi"].ToString();
                MaclarDrop();
                TribunAdDrop();
               
             
            }
        }

        public void MaclarDrop()
        {
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
                SqlCommand comma = new SqlCommand("select * from Maclar", con);
                SqlDataReader rader;
                try
                {
                    con.Open();
                    rader = comma.ExecuteReader();
                    DropDownList3.DataSource = rader;
                    DropDownList3.DataTextField = "Rakip";
                    DropDownList3.DataValueField = "MaID";
                    DropDownList3.DataBind();
                    rader.Close();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public void TribunAdDrop()
        {
            if (!IsPostBack)
            {
                SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
                SqlCommand comma = new SqlCommand("select * from TribunAdlari", con);
                SqlDataReader rader;
                try
                {
                    con.Open();
                    rader = comma.ExecuteReader();
                    DropDownList1.DataSource = rader;
                    DropDownList1.DataTextField = "TAdi";
                    DropDownList1.DataValueField = "TaID";
                    DropDownList1.DataBind();
                    rader.Close();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
                finally
                {
                    con.Close();
                }
            }
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int drsa = Convert.ToInt32(DropDownList3.SelectedValue);
            //if (drsa >= 1)
            //{
            //    SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
            //    SqlCommand comma = new SqlCommand("select * from Maclar WHERE MaID='" + (drsa - 1) + "'", con);
            //    SqlDataReader rader;
            //    try
            //    {
            //        con.Open();
            //        rader = comma.ExecuteReader();
            //        Label7.Text = rader.
            //        Label7.DataBind();
            //        rader.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        Response.Write(ex.ToString());
            //    }
            //    finally
            //    {
            //        con.Close();
            //    }
            //}
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int drsa = Convert.ToInt32(DropDownList1.SelectedValue);
            if (drsa >= 1)
            {
                SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
                SqlCommand comma = new SqlCommand("select * from TribunTip WHERE TaID='" + (drsa - 1) + "'", con);
                SqlDataReader rader;
                try
                {
                    con.Open();
                    rader = comma.ExecuteReader();
                    DropDownList2.DataSource = rader;
                    DropDownList2.DataTextField = "TtipAdi";
                    DropDownList2.DataValueField = "TtipID";
                    DropDownList2.DataBind();
                    rader.Close();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
                finally
                {
                    con.Close();
                }
            }
        }

        public string IngilizceTarihFormati(string GelenTarih)
        {
            try
            {
                DateTime Tarih = Convert.ToDateTime(GelenTarih);
                string Gun, Ay, Yil, Saat, Dakika, Saniye;
                Gun = Tarih.Day.ToString();
                Ay = Tarih.Month.ToString();
                Yil = Tarih.Year.ToString();
                Saat = Tarih.Hour.ToString();
                Dakika = Tarih.Minute.ToString();
                Saniye = Tarih.Second.ToString();
                string YeniTarih = Ay + "." + Gun + "." + Yil + " " + Saat + ":" + Dakika + ":" + Saniye;
                return YeniTarih;
            }
            catch (Exception HataKodu)
            {
              return GelenTarih;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int Macid = Convert.ToInt32(DropDownList3.SelectedValue) ;
            int TribunId = Convert.ToInt32(DropDownList1.SelectedValue);
            DateTime simdikitarih = DateTime.Now;
            string SatinAlmaTarih = IngilizceTarihFormati(simdikitarih.ToString());
            Response.Write(SatinAlmaTarih.ToString());
            string Sql = "INSERT INTO Satislar(KulId,MacId,TribunId,SatinAlmaTarih) VALUES('" + Session["kulId"] + "','" + Macid + "','" + TribunId + "','" + SatinAlmaTarih + "')";
            SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
            SqlCommand comand = new SqlCommand(Sql, con);
            try
            {
                con.Open();
                comand.ExecuteNonQuery();
                comand.Dispose();
                con.Close();
                con.Dispose();
                Response.Redirect("SatinAlinanBiletlerim.aspx");
            }
            catch (Exception)
            {
                Label8.Visible = true;
                Label8.ForeColor = System.Drawing.Color.Red;
                Label8.Text = "Satın Alma İşleminde bir Hata Oluştu!";
                
            }
        
        }

    
    }
}