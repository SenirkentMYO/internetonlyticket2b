﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace onlineTicket
{
    public partial class SatinAlinanBiletlerim : System.Web.UI.Page
    {
        public string ConnectionStringNesnesi()
        {
            try
            {
                string Providers = "Data Source=MINE-PC/MINE;Initial Catalog=onlineTicket;Integrated Security=True";
                return Providers;
            }
            catch (Exception ex)
            {
                Response.Write("DATABASE CONNECTİON HATA \n ------------------------------------------------------------------" + ex.ToString() + "\n ------------------------------------------------------------------");
                return null;
            }
        }


        public void yukle()
        {
          
            DataTable tablo = new DataTable();
            SqlDataAdapter getir = new SqlDataAdapter("SELECT Rakip as [Rakip Takım], TAdi as [Tribun Adi], SatinAlmaTarih [Satın Alma Tarihi] FROM Satislar , Maclar , TribunAdlari WHERE KulId='" + Session["kulId"] + "' and  MaID=MacId and  TaID=TribunId", ConnectionStringNesnesi());
            getir.Fill(tablo);
            GridView1.DataSource = tablo;
            GridView1.DataBind();
         
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["sayac"] == null)
            {
                Application["sayac"] = 1;
                Label.Text = Application["sayac"].ToString();
            }
            else
            {
                int sayi = (int)Application["sayac"];
                sayi++;
                Application["sayac"] = sayi;
                Label.Text = Application["sayac"].ToString();
            }
            if (Session["kulAdi"] == null)
            {
                Response.Redirect("hata.aspx");
            }
            else
            {
                Label2.Text = Session["kulAdi"].ToString();

                yukle();
            }
        }
    }
}