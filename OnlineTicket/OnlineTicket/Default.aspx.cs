﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OnlineTicket;

namespace onlineTicket
{
    public partial class Default : System.Web.UI.Page
    {
        public string ConnectionStringNesnesi()
        {
            try
            {
                string Providers = "Data Source=PC-BILGISAYAR;Initial Catalog=onlineTicket;Integrated Security=True";
                return Providers;
            }
            catch (Exception ex)
            {
                Response.Write("DATABASE CONNECTİON HATA \n ------------------------------------------------------------------" + ex.ToString() + "\n ------------------------------------------------------------------");
                return null;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        public string idal(string a , string b)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constring"].ToString());
            string sorgula = "SELECT KulID FROM Kullanicilar WHERE Kullanici='"+a+"' and Sifre='"+b+"'";
            con.Open();
            SqlCommand cmd = new SqlCommand(sorgula, con);
            int donenekayit = (int)cmd.ExecuteScalar();

            return donenekayit.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(ConnectionStringNesnesi());
            string sorgula = "SELECT COUNT(*) FROM Kullanicilar WHERE Kullanici=@Kullanici and Sifre=@Sifre";
            con.Open();
            SqlCommand cmd = new SqlCommand(sorgula, con);
         
            cmd.Parameters.Add("@Kullanici", SqlDbType.VarChar).Value = TextBox1.Text;
            cmd.Parameters.Add("@Sifre", SqlDbType.VarChar).Value = TextBox2.Text;
            int donenekayit = (int)cmd.ExecuteScalar();
            
            if (donenekayit <= 0)
            {
                Label3.Visible = true;
                Label3.ForeColor = System.Drawing.Color.Red;
                Label3.Text = "Kullanıcı Adı veya Şifre Hatalı!";
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else
            {
                Session["kulAdi"] = this.TextBox1.Text;
                Session["kulId"] = idal(TextBox1.Text, TextBox2.Text);
                Response.Redirect("uye.aspx");
                con.Close();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("sign.aspx");
        }
    }
}